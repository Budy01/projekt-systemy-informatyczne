import React from 'react';
import ReactDOM from 'react-dom';
import './config/firebase';
import './code/helpers/toCapitalize';
import {Application} from "./code/Application";

ReactDOM.render(<Application />, document.getElementById('root'));