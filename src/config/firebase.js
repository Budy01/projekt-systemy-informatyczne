import firebase from 'firebase';

const FIREBASE_CONFIG = {
    apiKey: "AIzaSyBvo6b7Oj3iqzjRs7wJhPoDjK1vsHTeUus",
    authDomain: "systemy-informatyczne.firebaseapp.com",
    databaseURL: "https://systemy-informatyczne.firebaseio.com",
    projectId: "systemy-informatyczne",
    storageBucket: "systemy-informatyczne.appspot.com",
    messagingSenderId: "198904625940"
};

firebase.initializeApp(FIREBASE_CONFIG);
