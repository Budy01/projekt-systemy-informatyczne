import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import UsersListAndSearchComponent from './components/UsersListAndSearchComponent';
import Layout from "./components/Layout";
import CreateNewUser from "./components/panels/CreateNewUser";
import ScheduleVisit from "./components/panels/ScheduleVisit";
import Profile from "./components/panels/Profile";
import Cures from "./components/panels/Cures";
import CssBaseline from "@material-ui/core/CssBaseline/index";
import Login from "./components/auth/Login";
import Loading from "./components/auth/Loading";
import {UserProvider} from "./services/user/UserProvider";
import {UserContext} from "./services/user/UserContext";
import Button from "@material-ui/core/Button";
import {UserAuthService} from "./services/UserAuthService";

export class Application extends React.Component {

    renderRouter = (user) => <Router>
        <Layout user={user}>
            <Switch>
                <Route path="/" component={() => <Profile user={user}/>} exact/>
                <Route path="/newUser" component={() => <CreateNewUser role={user.role}/>}/>
                <Route path="/cures" component={Cures}/>
                <Route path="/scheduleVisit" component={ScheduleVisit}/>
                <Route path="/users" component={UsersListAndSearchComponent}/>
            </Switch>
        </Layout>
    </Router>;

    render() {
        return <UserProvider>
            <UserContext.Consumer>
                {(user) => (<>
                    <CssBaseline/>
                    {{
                        undefined: <Loading/>,
                        true: this.renderRouter(user),
                        false: <Login/>
                    }[user.isLogged]}
                </>)}
            </UserContext.Consumer>

            {/*TODO: Only development purpose*/}
            <h1 style={{position: 'fixed', bottom: 0, right: 20}}>
                <Button onClick={() => UserAuthService.signInUser("patient@gmail.com", "123123")}>Patient</Button>
                <Button onClick={() => UserAuthService.signInUser("doctor@gmail.com", "123123")}>Doctor</Button>
            </h1>

        </UserProvider>
    }
}