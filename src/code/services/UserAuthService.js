import firebase from 'firebase';

class UserAuthService {

    createUser = (email, password) => firebase.auth()
            .createUserWithEmailAndPassword(email, password);

    signInUser = (email, password) => firebase.auth()
            .signInWithEmailAndPassword(email, password);


    signOutUser = () => firebase.auth()
            .signOut();

    getCurrentUser = () => firebase.auth().currentUser;
}

const instance = new UserAuthService();
Object.freeze(instance);

export {instance as UserAuthService};


