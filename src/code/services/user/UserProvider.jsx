import React from 'react';
import {UserContext} from "./UserContext";
import firebase from 'firebase';
import {DatabaseService} from "../DatabaseService";

export class UserProvider extends React.PureComponent {

    state = {};

    componentWillMount() {
        firebase.auth().onAuthStateChanged((user) => {
            if(user == null) {
                this.setState(() => ({isLogged: false}));
                return;
            }
            const {uid} = user;
            DatabaseService.collection('users').doc(uid).get().then(snapshot => {
                this.setState(() => ({
                    uid,
                    isLogged: !!user.uid,
                    ...snapshot.data()
                }));
            });
        });
    }

    render() {
        const {children} = this.props;
        return <UserContext.Provider value={this.state}>
            {children}
        </UserContext.Provider>
    }

}