import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import {SignOut} from './auth/SignOut';
import {Link} from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import Avatar from '@material-ui/core/Avatar';
import Medi_Icon from '../../img/medi_icon.png';
import TouchRipple from '@material-ui/core/ButtonBase';
import {Roles} from "../helpers/Roles";

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    grow: {
        flexGrow: 1,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    toolbar: theme.mixins.toolbar,
    signOutButton: {
        align: 'right'
    },
    image: {
        width: 50,
        height: 50
    },
    ripple: {
        borderRadius: 25
    }
});

class Layout extends React.Component {

    options = [
        {title: 'Profile', url: '/', roles: [Roles.DOCTOR, Roles.PATIENT]},
        {title: 'Create New User', url: '/newUser', roles: [Roles.DOCTOR]},
        {title: 'Schedule Visit', url: '/scheduleVisit', roles: [Roles.PATIENT]},
        {title: 'Cures', url: '/cures', roles: [Roles.PATIENT, Roles.DOCTOR]},
        {title: 'User List', url: '/users', roles: [Roles.DOCTOR]},
    ];

    renderOptions = () => this.options
        .filter(option => option.roles.includes(this.props.user.role))
        .map((option, index) =>
            <Link to={option.url} key={index} style={{textDecoration: 'none'}}>
                <ListItem button divider>
                    <ListItemText>
                        {option.title}
                    </ListItemText>
                </ListItem>
            </Link>);

    getHeader = () =>
        this.props.user
            ? `${this.props.user.role.toCapitalize()} ${this.props.user.firstName} ${this.props.user.lastName}`
            : `Unknown`;


    render() {
        const {classes, children} = this.props;
        return <div className={classes.root}>

            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar variant="dense">
                    <TouchRipple className={classes.ripple}>
                        <Avatar alt="Medi Icon" src={Medi_Icon} className={classes.image}/>
                    </TouchRipple>
                    <IconButton color="inherit" aria-label="Menu">
                        <AccountCircle/>
                    </IconButton>
                    <Typography variant="h6" color="inherit" className={classes.grow}>
                        {this.getHeader()}
                    </Typography>
                    <SignOut/>
                </Toolbar>

            </AppBar>


            <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.toolbar}/>
                <List component="nav">
                    {this.renderOptions()}
                </List>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.toolbar}/>
                {children}
            </main>
        </div>
    }

}

export default withStyles(styles)(Layout);




