import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress/index';
import {withStyles} from '@material-ui/core/styles/index';
import Avatar from '@material-ui/core/Avatar/index';
import Medi_Logo from '../../../img/medi_logo.png';

const styles = {
    main : {
        display       : 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        width         : 200,
        margin: 'auto'
    },
    image: {
        margin: 'auto',
        marginTop: 100,
        marginBottom: 35,
        width : 210,
        height: 210
    }
};

const Loading = ({classes}) => <div className={classes.main}>
    <Avatar alt="Medi Logo" src={Medi_Logo} className={classes.image}/>
    <LinearProgress/>
</div>;

export default withStyles(styles)(Loading);