import React, {Component} from 'react';
import Button from '@material-ui/core/Button/index';
import FormControlLabel from '@material-ui/core/FormControlLabel/index';
import Checkbox from '@material-ui/core/Checkbox/index';
import Paper from '@material-ui/core/Paper/index';
import Typography from '@material-ui/core/Typography/index';
import withStyles from '@material-ui/core/styles/withStyles';
import {UserAuthService} from '../../services/UserAuthService';
import TextField from "@material-ui/core/TextField";

const styles = theme => ({
    main  : {
        width                                                   : 'auto',
        display                                                 : 'block', // Fix IE 11 issue.
        marginLeft                                              : theme.spacing(3),
        marginRight                                             : theme.spacing(3),
        [theme.breakpoints.up(400 + theme.spacing(3 * 2))]: {
            width      : 400,
            marginLeft : 'auto',
            marginRight: 'auto',
        },
    },
    paper : {
        marginTop    : theme.spacing(8),
        display      : 'flex',
        flexDirection: 'column',
        alignItems   : 'center',
        padding      : `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`,
    },
    avatar: {
        margin         : theme.spacing(),
        backgroundColor: theme.palette.secondary.main,
    },
    form  : {
        width    : '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(),
    },
    submit: {
        marginTop: theme.spacing(3),
    },
});

class Login extends Component {

    state = {};

    componentDidMount() {
        var htmlInput = document.getElementById('email');
        htmlInput.oninvalid = function(e) {
            e.target.setCustomValidity('Enter login and password');
        };
        htmlInput.oninput = function(e) {
            e.target.setCustomValidity(''); //cleans validation!
        };

        var htmlInput2 = document.getElementById('password');
        htmlInput2.oninvalid = function(e) {
            e.target.setCustomValidity('Enter login and password');
        };
        htmlInput2.oninput = function(e) {
            e.target.setCustomValidity(''); //cleans validation!
        };

    }

    handleSubmit = event => {
        event.preventDefault();
        UserAuthService.signInUser(this.state.email, this.state.password)
            .catch((error) => this.setState(()=>({message: error.message})));
    };

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const {classes} = this.props;

        return (
            <main className={classes.main}>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        User login
                    </Typography>
                    <form className={classes.form} onSubmit={this.handleSubmit}>
                        <TextField
                            id="email"
                            name="email"
                            label="Email address"
                            onChange={this.handleChange}
                            margin="normal"
                            type="email"
                            autoComplete="email"
                            required
                            fullWidth
                        />
                        <TextField
                            id="password"
                            name="password"
                            label="Password"
                            onChange={this.handleChange}
                            margin="normal"
                            type="password"
                            autoComplete="current-password"
                            required
                            fullWidth
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary"/>}
                            label="Remember me"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            LOG IN
                        </Button>
                    </form>
                    {this.state.message && this.state.message}
                </Paper>
            </main>
        );
    }
}

export default withStyles(styles)(Login);