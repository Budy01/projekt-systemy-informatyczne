import React from 'react';
import {UserAuthService} from '../../services/UserAuthService';
import Button from '@material-ui/core/Button/index';

export const SignOut = (props) =>
    <Button color="inherit" onClick={UserAuthService.signOutUser} className={props.className}>
        Sign Out
    </Button>;