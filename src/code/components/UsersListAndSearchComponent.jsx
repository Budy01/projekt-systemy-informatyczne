import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {DatabaseService} from '../services/DatabaseService';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    button: {
        // margin: theme.spacing.unit,
    },
    root: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        [theme.breakpoints.up(400 + theme.spacing(6))]: {
            width: 1200,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    search: {
        width: '100%'
    },
    table: {
        // minWidth: 500,
    },
});

class UsersListAndSearchComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            tmp: [] // Niezbędna w onChange(e) - przechowuje dane z bazy i nie jest modyfikowana.
        };

        this.onChange = this.onChange.bind(this);

        DatabaseService.collection('users').get().then(snapshot => {
            let array = [];
            snapshot.forEach(doc => array.push(doc.data()));
            this.setState(() => ({users: array}));
            this.setState(() => ({tmp: array}));
        });
    }

    onChange(e) {
        this.setState(
            {
                users: this.state.tmp.filter(
                    function (user) {
                        return user.role.toLowerCase().includes(e.target.value.toLowerCase()) ||
                            user.firstName.toLowerCase().includes(e.target.value.toLowerCase()) ||
                            user.lastName.toLowerCase().includes(e.target.value.toLowerCase())
                    }
                )
            }
        );

    }

    render() {
        const {classes} = this.props;
        const {users} = this.state;

        return (
            <div className={classes.root}>
                <Grid container justify="center">
                    <TextField
                        InputProps={{height: 30}}
                        className={classes.search}
                        placeholder="User search"
                        variant="outlined"
                        onChange={this.onChange}
                    />
                </Grid>

                <br/><br/>

                <Paper>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                {["Name", "Gender", "Birth Date", "Role", "Email", "Pesel"].map((label) =>
                                    <TableCell>{label}</TableCell>)}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.map(({firstName, role, lastName, email, gender, date, pesel}, index) => (
                                <TableRow key={index}>
                                    <TableCell>{firstName} {lastName}</TableCell>
                                    <TableCell>{gender}</TableCell>
                                    <TableCell>{date}</TableCell>
                                    <TableCell>{role}</TableCell>
                                    <TableCell>{email}</TableCell>
                                    <TableCell>{pesel}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(UsersListAndSearchComponent);
