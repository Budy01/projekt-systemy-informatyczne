import React from 'react';
import moment from "moment/moment";
import {UserAuthService} from "../../services/UserAuthService";
import {getDoctorFullName} from "../../helpers/getDoctorFullName";
import {DatabaseService} from '../../services/DatabaseService';
import Paper from "@material-ui/core/Paper/index";
import {withStyles} from "@material-ui/core/index";
import Typography from "@material-ui/core/Typography/index";
import TextField from "@material-ui/core/TextField/index";
import Grid from "@material-ui/core/Grid/index";
import {DateTimePicker, MuiPickersUtilsProvider,} from "@material-ui/pickers/index";
import MomentUtils from '@date-io/moment/build/index';
import Button from "@material-ui/core/Button/index";

const styles = theme => ({
    root: {
        marginBottom: theme.spacing(3),
        padding: theme.spacing(3),
        [theme.breakpoints.up(600 + theme.spacing(6))]: {
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        }
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(),
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
});

class RequestVisit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dateTime: moment()
        }
    }

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    setDateTime = (dateTime) => {
        this.setState(() => ({dateTime}))
    };

    submit = (e) => {
        this.setState(() => ({isLoading: true}));
        e.preventDefault();
        const patientId = UserAuthService.getCurrentUser().uid;
        const date = this.state.dateTime.format();
        const doctor = this.props.doctor;
        const description = this.state.description;
        DatabaseService.collection("visits").add({
            patientId,
            date,
            doctor,
            description
        }).then(() => {
            this.setState(() => ({success: "Success"}))
        }, (error) => {
            this.setState(() => ({error: error}));
        }).finally(() => this.setState(() => ({isLoading: false})));
    };

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState(({success: undefined}));
    }

    render() {
        const {dateTime, error, isLoading, success} = this.state;
        const {doctor, classes} = this.props;
        return success ?
            <Paper className={classes.root}>
                {success}
            </Paper> : doctor
                ? <Paper className={classes.root}>
                    <Typography component="h1" variant="h5" align="center" className={classes.header}>
                        Schedule visit to {getDoctorFullName(doctor)}
                    </Typography>
                    <Typography component="p" align="center" className={classes.header}>
                        {doctor.specialization}
                    </Typography>

                    <form onSubmit={this.submit}>
                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={24}>

                                <MuiPickersUtilsProvider utils={MomentUtils}>
                                    <DateTimePicker name="dateTime"
                                                    value={dateTime}
                                                    onChange={this.setDateTime}
                                                    label="Date picker"
                                                    fullWidth/>
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={12} sm={24}>
                                <TextField
                                    id="description"
                                    name="description"
                                    label="Description"
                                    onChange={this.handleChange}
                                    margin="normal"
                                    required
                                    fullWidth
                                />

                                <div className={classes.buttons}>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        className={classes.button}
                                        type="submit"
                                        disabled={isLoading}
                                    >
                                        Submit
                                    </Button>
                                </div>
                            </Grid>
                        </Grid>
                    </form>

                    {error}
                </Paper>
                : null;
    }
}

export default withStyles(styles)(RequestVisit);