import React from 'react';
import {withStyles} from '@material-ui/core/styles/index';
import Table from '@material-ui/core/Table/index';
import TableBody from '@material-ui/core/TableBody/index';
import TableCell from '@material-ui/core/TableCell/index';
import TableHead from '@material-ui/core/TableHead/index';
import TableRow from '@material-ui/core/TableRow/index';
import Paper from '@material-ui/core/Paper/index';
import {DatabaseService} from '../../services/DatabaseService';
import Button from '@material-ui/core/Button/index';
import TextField from '@material-ui/core/TextField/index';
import Grid from '@material-ui/core/Grid/index';
import RequestVisit from "./RequestVisit";
import {getDoctorFullName} from "../../helpers/getDoctorFullName";

const styles = theme => ({
    root: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        [theme.breakpoints.up(400 + theme.spacing(3 * 2))]: {
            width: 1000,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    search: {
        width: '100%',
        backgroundColor: theme.palette.common.white
    },
});

class ScheduleVisit extends React.Component {

    doctors = [];

    constructor(props) {
        super(props);
        this.state = {
            doctors: [],
            tmp: []
        };
        DatabaseService.collection('users').where('role', '==', 'doctor').get().then(snapshot => {
            let array = [];
            snapshot.forEach(doc => array.push(doc.data()));
            this.doctors = array;
            this.setState(() => ({doctors: array}));
        });
    }

    filterDoctors = (doctors, value) => doctors.filter((doctor) => {
            const specializationFilter = doctor.specialization && doctor.specialization.toLowerCase().includes(value.toLowerCase());
            const firstNameFilter = doctor.firstName && doctor.firstName.toLowerCase().includes(value.toLowerCase());
            const lastNameFilter = doctor.lastName && doctor.lastName.toLowerCase().includes(value.toLowerCase());
            return specializationFilter || firstNameFilter || lastNameFilter;
        }
    );

    onChange = (e) => {
        this.setState({doctors: this.filterDoctors(this.doctors, e.target.value)})
    };

    createScheduleButtonAction = (doctor) => () => {
        this.setState(() => ({activeDoctor: doctor}))
    };

    render() {
        const {classes} = this.props;
        const {doctors, activeDoctor} = this.state;
        return (
            <div className={classes.root}>
                <Grid container justify="center">
                    <TextField
                        InputProps={{height: 30}}
                        className={classes.search}
                        placeholder="Search"
                        variant="outlined"
                        onChange={this.onChange}
                    />
                </Grid>
                <br/>

                <RequestVisit doctor={activeDoctor}/>

                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Doctor Name</TableCell>
                                <TableCell>Specialization</TableCell>
                                <TableCell>Rating</TableCell>
                                <TableCell>Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {doctors.map(doctor => (
                                <TableRow key={doctor.id}>
                                    <TableCell>{getDoctorFullName(doctor)}</TableCell>
                                    <TableCell component="th" scope="row">
                                        {doctor.specialization}
                                    </TableCell>
                                    <TableCell>{Math.ceil(Math.random() * 5)}.0</TableCell>
                                    <TableCell>

                                        <Button variant="outlined" color="primary" size="small"
                                                onClick={this.createScheduleButtonAction(doctor)}>
                                            Schedule Visit
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>


            </div>
        );
    }
}

export default withStyles(styles)(ScheduleVisit);