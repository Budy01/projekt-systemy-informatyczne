import React from 'react';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles/index';
import Table from '@material-ui/core/Table/index';
import TableBody from '@material-ui/core/TableBody/index';
import TableCell from '@material-ui/core/TableCell/index';
import TableHead from '@material-ui/core/TableHead/index';
import TablePagination from '@material-ui/core/TablePagination/index';
import TableRow from '@material-ui/core/TableRow/index';
import TableSortLabel from '@material-ui/core/TableSortLabel/index';
import Toolbar from '@material-ui/core/Toolbar/index';
import Typography from '@material-ui/core/Typography/index';
import Paper from '@material-ui/core/Paper/index';
import Checkbox from '@material-ui/core/Checkbox/index';
import IconButton from '@material-ui/core/IconButton/index';
import Tooltip from '@material-ui/core/Tooltip/index';

import FilterListIcon from '@material-ui/icons/FilterList';
import Button from '@material-ui/core/Button/index';
import {lighten} from '@material-ui/core/styles/colorManipulator';

let counter = 0;
function createData(name, symptoms, paracetamol, ibuprofen, side_effects) {
    counter += 1;
    return { id: counter, name, symptoms: symptoms, paracetamol: paracetamol, ibuprofen, side_effects };
}

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    { id: 'name', numeric: false, disablePadding: true, label: 'Name' },
    { id: 'symptoms', text: true, disablePadding: false, label: 'symptoms' },
    { id: 'paracetamol', numeric: true, disablePadding: false, label: 'paracetamol (mg)' },
    { id: 'ibuprofen', numeric: true, disablePadding: false, label: 'ibuprofen (mg)' },
    { id: 'side_effects', text: true, disablePadding: false, label: 'side_effects' },
];

class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const { onSelectAllClick, order, orderBy, numSelected, rowCount } = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <Checkbox
                            indeterminate={numSelected > 0 && numSelected < rowCount}
                            checked={numSelected === rowCount}
                            onChange={onSelectAllClick}
                        />
                    </TableCell>
                    {rows.map(
                        row => (
                            <TableCell
                                key={row.id}
                                //align={row.numeric ? 'left' : 'right'}
                                align={row.text ? 'right' : 'left'}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === row.id}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        ),
                        this,
                    )}
                </TableRow>
            </TableHead>
        );
    }
}

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing(),
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
});

let EnhancedTableToolbar = props => {
    const { numSelected, classes } = props;

    return (
        <Toolbar
            className={classNames(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            <div className={classes.title}>
                {numSelected > 0 ? (
                    <Typography color="inherit" variant="subtitle1">
                        {numSelected} selected
                    </Typography>
                ) : (
                    <Typography variant="h6" id="tableTitle">
                        Drugs
                    </Typography>
                )}
            </div>
            <div className={classes.spacer} />
            <div className={classes.actions}>
                {numSelected > 0 ? (
                    <Tooltip title="Edit">
                        <IconButton aria-label="create-outline">
                            <Button variant="contained" className={classes.button}>
                                Edit
                            </Button>
                        </IconButton>
                    </Tooltip>
                ) : (
                    <Tooltip title="Filter list">
                        <IconButton aria-label="Filter list">
                            <FilterListIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
    root: {
        width: '100%',
        // marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: 1020,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});

class EnhancedTable extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'name',
        selected: [],
        data: [
            createData('Abacavir ','fever', 3.7, 67, 'Nausea'),
            createData('Allopurinol', 'leukemia', 25.0, 51, 'Headache'),
            createData('Busulfan', 'cancer', 16.0, 24, 'Mouth sores'),
            createData('Calcium', 'hypocalcemia', 6.0, 24, 'Feeling tired'),
            createData('Captopril', 'high pressure', 16.0, 49, 'Cough'),
            createData('Dapsone', 'pneumonia', 3.2, 87, 'Stomach upset'),
            createData('Daunorubicin', 'cancer', 9.0, 37, 'Loss of appetite'),
            createData('Eltrombopag', 'thrombocytopenia', 0.0, 94, 'Trouble sleeping'),
            createData('Emicizumab', 'bleeding', 26.0, 65, 'Joint or muscle pain'),
            createData('Fluconazole', 'fungal infections', 0.2, 98, 'Nausea and vomiting'),
            createData('Fluorouracil', 'cancer', 0, 81, 'Bruising'),
            createData('Gefitinib', 'cancer', 19.0, 9, 'Acne'),
            createData('Gemcitabine', 'cancer', 18.0, 63, 'Feeling tired or weak'),
            createData('Hydralazine', 'low blood pressure', 19.0, 9, 'Retaining sodium and water'),
            createData('Hydroxyurea', 'cancer', 19.0, 9, 'Rash and itching'),
            createData('Imatinib', 'cancer', 19.0, 9, 'Abnormal liver tests'),
            createData('Irinotecan', 'cancer', 19.0, 9, 'Rash'),
            createData('Ketoconazole', 'fungal infections', 19.0, 9, 'Hair loss'),
            createData('Labetalol', 'high blood pressure', 19.0, 9, 'Low blood pressure'),
            createData('Meloxicam', 'joint pain', 19.0, 9, 'Heartburn'),
            createData('Nevirapine', 'HIV infection', 19.0, 9, 'Rash'),
            createData('Omeprazole', 'stomach ulcers', 19.0, 9, 'Feeling dizzy'),
            createData('Penicillin VK', 'infections', 19.0, 9, 'Headache'),
            createData('Phenytoin', 'migraine', 19.0, 9, 'Nausea, vomiting'),
            createData('Ranitidine', 'stomach ulcers', 19.0, 9, 'Feeling drowsy'),
            createData('Rituximab', 'cancer', 19.0, 9, 'Flu-like reaction'),
            createData('Stavudine', 'HIV infection', 19.0, 9, 'Rash'),
            createData('Teniposide', 'cancer', 19.0, 9, 'Loss of appetite'),
            createData('Vincristine', 'cancer', 19.0, 9, 'Rash'),
            createData('Warfarin', 'blood clotting', 19.0, 9, 'Unusual bruising'),
            createData('Zidovudine', 'HIV infection', 19.0, 9, 'Headache'),


        ],
        page: 0,
        rowsPerPage: 5,
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({ order, orderBy });
    };

    handleSelectAllClick = event => {
        if (event.target.checked) {
            this.setState(state => ({ selected: state.data.map(n => n.id) }));
            return;
        }
        this.setState({ selected: [] });
    };

    handleClick = (event, id) => {
        const { selected } = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({ selected: newSelected });
    };

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    render() {
        const { classes } = this.props;
        const { data, order, orderBy, selected, rowsPerPage, page } = this.state;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

        return (
            <Paper className={classes.root}>
                <EnhancedTableToolbar numSelected={selected.length} />
                <div className={classes.tableWrapper}>
                    <Table className={classes.table} aria-labelledby="tableTitle">
                        <EnhancedTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={this.handleSelectAllClick}
                            onRequestSort={this.handleRequestSort}
                            rowCount={data.length}
                        />
                        <TableBody>
                            {stableSort(data, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(n => {
                                    const isSelected = this.isSelected(n.id);
                                    return (
                                        <TableRow
                                            hover
                                            onClick={event => this.handleClick(event, n.id)}
                                            role="checkbox"
                                            aria-checked={isSelected}
                                            tabIndex={-1}
                                            key={n.id}
                                            selected={isSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox checked={isSelected} />
                                            </TableCell>
                                            <TableCell component="th" scope="row" padding="none">
                                                {n.name}
                                            </TableCell>
                                            <TableCell align="right">{n.symptoms}</TableCell>
                                            <TableCell align="right">{n.paracetamol}</TableCell>
                                            <TableCell align="right">{n.ibuprofen}</TableCell>
                                            <TableCell align="right">{n.side_effects}</TableCell>
                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 49 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Next Page',
                    }}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </Paper>
        );
    }
}

export default withStyles(styles)(EnhancedTable);