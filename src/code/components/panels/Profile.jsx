import React, {Component} from 'react';
import Paper from '@material-ui/core/Paper/index';
import Typography from '@material-ui/core/Typography/index';
import withStyles from '@material-ui/core/styles/withStyles';
import Avatar from "@material-ui/core/Avatar/index";
import TextField from "@material-ui/core/TextField/index";
import Table from "@material-ui/core/Table/index";
import TableHead from "@material-ui/core/TableHead/index";
import TableRow from "@material-ui/core/TableRow/index";
import TableCell from "@material-ui/core/TableCell/index";
import TableBody from "@material-ui/core/TableBody/index";
import {DatabaseService} from "../../services/DatabaseService";
import {getDoctorFullName} from "../../helpers/getDoctorFullName";
import moment from "moment/moment";
import {Roles} from "../../helpers/Roles";
import Button from "@material-ui/core/Button";
import { Check, Close } from '@material-ui/icons';

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        [theme.breakpoints.up(400 + theme.spacing(6))]: {
            width: 1000,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`,
    },
    image: {
        margin: 'auto',
        marginTop: 10,
        marginBottom: 10,
        width: 150,
        height: 150,
    },
    textField: {
        marginLeft: theme.spacing(),
        marginRight: theme.spacing(),
        width: 150,
        marginTop: 20,
    },
});

class Profile extends Component {

    constructor(props) {
        super(props);

        this.getVisits();

        this.state = {
            diseases: [
                {
                    date: "2018-05-06",
                    disease: "Headache",
                    doctor: "John Nowak",
                    description: "Headaches can be primary, with their stand-alone cause, or secondary when              another medical condition or a person's lifestyle has induced them."
                },
                {
                    date: "2015-12-22",
                    disease: "Fever",
                    doctor: "Maria Nowak",
                    description: "A fever is a temporary increase in your body temperature, often due to an                illness. Having a fever is a sign that something out of the ordinary is going on in your body."
                },
                {
                    date: "2012-01-03",
                    disease: "Flu",
                    doctor: "Jane Doe",
                    description: "The flu is a disease that's easily spread between people. When you have body                  aches, a fever, and a sore throat, you probably have the flu."
                }],
            visits: []
        };
    }

    getVisits = () => {
        const {user} = this.props;
        const {fieldPath, opStr, value} = user.role === Roles.DOCTOR
            ? {fieldPath: 'doctor.email', opStr: '==', value: user.email}
            : {fieldPath: 'patientId', opStr: '==', value: user.uid};
        this.visitsUnsubscribe = DatabaseService.collection('visits').where(fieldPath, opStr, value).onSnapshot(snapshot => {
            let array = [];
            snapshot.forEach(doc => array.push({...doc.data(), id: doc.id}));
            this.setState(() => ({visits: array}));
        });
    };

    acceptVisit = (id) => {
        DatabaseService.collection('visits').doc(id).update({status: "Accepted"}).then(() => console.log(`Visit ${id} accepted`));
    };
    declineVisit = (id) => {
        DatabaseService.collection('visits').doc(id).update({status: "Declined"}).then(() => console.log(`Visit ${id} declined`));
    };

    componentWillUnmount() {
        this.visitsUnsubscribe();
    }

    render() {
        const {classes, user = {}} = this.props;

        return (
            <main className={classes.main}>
                <Paper className={classes.paper}>
                    <Avatar src="http://placekitten.com/144/144" className={classes.image}/>
                    <Typography component="h1" variant="h3" align="center">
                        {user.role.toCapitalize()}
                    </Typography>

                    <div align="center">
                        <TextField
                            label="First name"
                            className={classes.textField}
                            value={user.firstName}
                            margin="normal"
                            InputProps={{readOnly: true}}
                        />

                        <TextField
                            label="Last name"
                            defaultValue={user.lastName}
                            className={classes.textField}
                            margin="normal"
                            InputProps={{readOnly: true}}
                        />

                        <TextField
                            label="PESEL"
                            defaultValue={user.pesel}
                            className={classes.textField}
                            margin="normal"
                            InputProps={{readOnly: true}}
                        />

                        <TextField
                            label="Date of birth"
                            defaultValue={user.date}
                            className={classes.textField}
                            margin="normal"
                            InputProps={{readOnly: true}}
                        />

                        <TextField
                            label="Sex"
                            defaultValue={user.gender}
                            className={classes.textField}
                            margin="normal"
                            InputProps={{readOnly: true}}
                        />
                    </div>
                </Paper>
                <br/><br/>
                {this.state.visits.length ?
                    <>
                        <Paper>
                            <br/>
                            <Typography component="h1" variant="h5" align="center">
                                Appointments Requests
                            </Typography>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center"><b>Doctor</b></TableCell>
                                        <TableCell align="center"><b>Specialization</b></TableCell>
                                        <TableCell align="center"><b>Description</b></TableCell>
                                        <TableCell><b>Date</b></TableCell>
                                        <TableCell><b>Time</b></TableCell>
                                        <TableCell
                                            align="center"><b>{user.role === Roles.DOCTOR ? "Actions " : "Status"}</b></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.visits.map(({doctor, status, date, description, id}, index) => (
                                        <TableRow key={index}>
                                            <TableCell align="center">{getDoctorFullName(doctor)}</TableCell>
                                            <TableCell align="center">{doctor.specialization}</TableCell>
                                            <TableCell align="center">{description}</TableCell>
                                            <TableCell component="th"
                                                       scope="row">{moment(date).format("MMMM Do YYYY")}</TableCell>
                                            <TableCell component="th"
                                                       scope="row">{moment(date).format("h:mm")}</TableCell>
                                            <TableCell align="center">{
                                                user.role === Roles.PATIENT
                                                    ? status
                                                        ? status
                                                        : "Pending"
                                                    : status
                                                        ? <span>{`Already ${status}`}</span>
                                                        : <div>
                                                        <Button style={{marginRight: '5px'}} color="primary" variant="contained" size="small" onClick={() => this.acceptVisit(id)}><Check fontSize="small"/></Button>
                                                        <Button color="primary" variant="contained" size="small" onClick={() => this.declineVisit(id)}><Close fontSize="small"/></Button>
                                                    </div>

                                            }</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Paper>
                        < br/> < br/>
                    </> : null}

                <Paper>
                    <br/>
                    <Typography component="h1" variant="h5" align="center">
                        Medical history
                    </Typography>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Date</b></TableCell>
                                <TableCell align="center"><b>Disease</b></TableCell>
                                <TableCell align="center"><b>Doctor</b></TableCell>
                                <TableCell align="center"><b>Description</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.diseases.map((n, index) => (<TableRow key={index}>
                                    <TableCell component="th" scope="row">{n.date}</TableCell>
                                    <TableCell align="left">{n.disease}</TableCell>
                                    <TableCell align="left">{getDoctorFullName(n.doctor)}</TableCell>
                                    <TableCell align="left">{n.description}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <br/>
            </main>
        );
    }
}

export default withStyles(styles)(Profile);