import React from 'react';
import Grid from '@material-ui/core/Grid/index';
import Typography from '@material-ui/core/Typography/index';
import TextField from '@material-ui/core/TextField/index';
import FormControlLabel from '@material-ui/core/FormControlLabel/index';
import Radio from '@material-ui/core/Radio/index';
import RadioGroup from '@material-ui/core/RadioGroup/index';
import FormLabel from '@material-ui/core/FormLabel/index';
import Paper from '@material-ui/core/Paper/index';
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button/index';
import {UserAuthService} from '../../services/UserAuthService';
import {DatabaseService} from '../../services/DatabaseService'
import MenuItem from "@material-ui/core/MenuItem/index";
import Select from "@material-ui/core/Select/index";

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(4))]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    header: {
        marginBottom: theme.spacing()
    },
    paper: {
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(6))]: {
            // marginTop   : theme.spacing.unit * 6,
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(),
    },
});

class CreateNewUser extends React.Component {

    initialState = {
        firstName: '',
        lastName: '',
        pesel: '',
        email: '',
        gender: 'female',
        date: '',
        password: '',
        role: 'patient',
        isLoading: false,
        specialization: '',
    };

    state = this.initialState;

    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState(() => ({isLoading: true}));
        UserAuthService.createUser(this.state.email, this.state.password)
            .then(
                ({user}) => {
                    const {password, isLoading, message, ...data} = this.state;
                    DatabaseService.collection('users').doc(user.uid).set({...data})
                        .then(() => {
                                this.setState(() => ({
                                    ...this.initialState,
                                    message: `User created successfully!`
                                }));
                            },
                            (error) => {
                                this.setState(() => ({isLoading: false, message: error.message}))
                            });

                },
                (error) => {
                    this.setState(() => ({isLoading: false, message: error.message}))
                });
    };

    render() {
        const {classes} = this.props;
        return (
            <main className={classes.layout}>
                <form onSubmit={this.handleSubmit}>
                    <Paper className={classes.paper}>
                        <Typography component="h1" variant="h4" align="center" className={classes.header}>
                            Create New User
                        </Typography>
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={24}>
                                <Select
                                    name="role"
                                    value={this.state.role}
                                    onChange={this.handleChange}
                                    fullWidth
                                    inputProps={{
                                        name: 'role',
                                    }}
                                    style={{paddingBottom: 0}}
                                >
                                    <MenuItem value={"patient"}>Patient</MenuItem>
                                    <MenuItem value={"doctor"}>Doctor</MenuItem>
                                    {this.props.role === "doctor" &&
                                    <MenuItem value={"manager"} disabled>Manager</MenuItem>}
                                    <MenuItem value={"accountant"} disabled>Accountant</MenuItem>
                                    <MenuItem value={"logistic"} disabled>Logistic</MenuItem>
                                </Select>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="firstName"
                                    name="firstName"
                                    label="First name"
                                    fullWidth
                                    value={this.state.firstName}
                                    onChange={this.handleChange}
                                    inputProps={{minLength: 3}}
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="lastName"
                                    name="lastName"
                                    label="Last name"
                                    fullWidth
                                    value={this.state.lastName}
                                    onChange={this.handleChange}
                                    inputProps={{minLength: 3}}
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="pesel"
                                    name="pesel"
                                    label="PESEL"
                                    fullWidth
                                    value={this.state.pesel}
                                    onChange={this.handleChange}
                                    inputProps={{minLength: 11, maxLength: 11}}
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="email"
                                    name="email"
                                    label="E-mail"
                                    fullWidth
                                    value={this.state.email}
                                    onChange={this.handleChange}
                                    type="email"
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="date"
                                    name="date"
                                    label="Birthday"
                                    type="date"
                                    className={classes.textField}
                                    onChange={this.handleChange}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormLabel component="legend">Gender</FormLabel>
                                <RadioGroup
                                    aria-label="gender"
                                    name="gender"
                                    style={{display: 'inline-block'}}
                                    value={this.state.gender}
                                    onChange={this.handleChange}
                                    required
                                >
                                    <FormControlLabel value="female"
                                                      control={<Radio color="primary"/>}
                                                      label="Female"/>
                                    <FormControlLabel value="male"
                                                      control={<Radio color="primary"/>}
                                                      label="Male"/>
                                </RadioGroup>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="password"
                                    name="password"
                                    label="Password"
                                    type="password"
                                    fullWidth
                                    value={this.state.password}
                                    onChange={this.handleChange}
                                    inputProps={{minLength: 6}}
                                    required
                                />
                            </Grid>
                            {this.state.role === 'doctor'
                                ? <Grid item xs={12} sm={6}>
                                    <TextField
                                        id="specialization"
                                        name="specialization"
                                        label="Specialization"
                                        fullWidth
                                        value={this.state.specialization}
                                        onChange={this.handleChange}
                                        inputProps={{minLength: 3}}
                                        required
                                    />
                                </Grid> : null}
                        </Grid>

                        <div className={classes.buttons}>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                type="submit"
                                disabled={this.state.isLoading}
                            >
                                Submit
                            </Button>
                        </div>
                        {this.state.message && this.state.message}
                    </Paper>
                </form>
            </main>
        );
    }
}

export default withStyles(styles)(CreateNewUser);