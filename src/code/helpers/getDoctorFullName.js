export const getDoctorFullName = (doctor) =>
    doctor ? `MD ${doctor.firstName} ${doctor.lastName}`
        : "Unknown Name";