## APLIKACJA DO ZARZĄDZANIA PLACÓWKĄ MEDYCZNĄ

### Story:
1. Logowanie do systemu. Różne role i uzależnione od tego widoki; 
   Widok: doktora, kierownika, pacjenta, księgowa;
2. Dodawanie nowych pacjentów oraz edycja istniejących do bazdy danych. Panel wyświetlający informacje o danym pacjęcie (dane osobowe), historie wizy oraz chorób. Uprawnienia dostępu: kierownik, doktor.
3. Dodawanie i edycja doktorów do bazy placówki medycznej. Wyszukiwarka po stanowisku oraz panel wyświetlający dane pracownika wraz z harmonogramem jego pracy na najbliższy tydzień, miesiąc, rok (tu dopuszczalne puste pola). Uprawnienia dostępu: kierownik; doktor
4. Ustalanie terminu wizyty w placówce medycznej wraz opisem. Automatyczne tworzenie karty pacjenta po ustaleniu wizyty wraz z danymi. Dodanie potrzebnych lekarstw po wizycie
Uprawnienia: kierownik, doktor, pacjent
5. Wyszukiwarka wykonanych badań, obsłużonych klientów, terminów wykonania usługi.
Uprawnienia: kierownik
6. Widok potrzebnych medykamentów, zamawianie oraz edycja produktów. Generowanie zamówienia do hurtowni.
Uprawnienia:
kierownik, logistyk
7. Historia zmian w programie - wszystkie zmiany zapisywane w bazie danych. Panel wyświetlania zmian. Wyszukiwarka po dacie, trybie zmiany (dodawanie, edycja) oraz prawach dostępu np. wyszukaj wszystkie
zmiany jakich dokonał doktor;
Uprawnienia: kierownik;
8. Panel księgowej. Uprawnienia: kierownik tryb odczytu) i księgowa tryb edycji. Wyszukiwarka wykonanych usług
po: cenie usługi, dacie usługi, cenie kosztów zewnętrznych urządzeń, Po wyszukaniu informacji z bazy na temat usługi (zestawu usług) możliwość wygenerowania raportu w formacie pdf z wybranymi
polami;

### Aktorzy:

- Pacjent: imie, nazwisko, PESEL, data urodzenia, płeć, historia choroby
- Lekarz: imię, nazwisko, ile lat doświadczenia, ile lat w firmie, kalendarz pracy;
- Kierownik: imię, nazwisko, ile lat doświadczenia, ile lat w firmie,
kalendarz pracy;
- Logistyk: imię, nazwisko, ile lat doświadczenia, ile lat w firmie,
kalendarz pracy;
- Księgowa: imię, nazwisko, ile lat doświadczenia, ile lat w firmie;

### "Use Case" Diagram - Diagram przypadków użycia:
https://www.lucidchart.com/invitations/accept/7c7234a0-164b-4339-9b38-59f35d174cff

## Git & CheatSheets

### CheatSheets
- [JavaScript ES6 CheatSheet](https://devhints.io/es6)
- [React CheatSheet](https://devhints.io/react)


### GIT
To są wszystkie komendy z jakich ja korzystam. Wszystko da się zrobić z IntelliJ, szczególnie polecam Commity ;)  
#### `git pull` - pobiera najnowsze zmiany
#### `git checkout master` - przełącza się na główny branch
#### `git checkout -b wk/hisotryka` - tworzy nowy branch i przełącza się na niego, możemy się umówić że będziemy je nazywać `inicjały/nr historyjki`
#### `git add .` - dodaje wszystkie zmienione pliki
#### `git commit -m "Opis commita"` - zapisuje pliki które zostały wcześniej dodane
#### `git push -u origin wk/historyjka` - wrzuca nasz branch do repozytorium
#### `git merge wk/historyjka` - łączy nasze branch w jeden 
#### `git status` - aktualny status  
#### `git checkout -f` - usuwa wszystkie zmiany i przywraca do stanu "czystego"

## This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
### Requirements
- [NodeJS](https://nodejs.org/en/)

### Available Scripts

In the project directory, you can run:

### `npm install`

Install all needed dependencies.

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

